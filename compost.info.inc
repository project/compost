<?php
/**
 * @file
 * Entity information for the Command Post module.
 */


class CompostSiteStatusViewsController extends EntityDefaultViewsController {

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function views_data() {
    $data = parent::views_data();

    $data['compost_site_status']['last_message']['field']['handler'] = 'views_handler_field_xss';

    return $data;
  }
}


class CompostAlertViewsController extends EntityDefaultViewsController {

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function views_data() {
    $data = parent::views_data();

    $data['compost_alert']['description']['field']['handler'] = 'views_handler_field_xss';
    $data['compost_alert']['group']['filter']['handler'] = 'views_handler_filter_group';

    return $data;
  }
}


class CompostCommandViewsController extends EntityDefaultViewsController {

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function views_data() {
    $data = parent::views_data();

    $data['compost_command']['description']['field']['handler'] = 'views_handler_field_xss';
    $data['compost_command']['group']['filter']['handler'] = 'views_handler_filter_group';

    return $data;
  }
}


class CompostVariableViewsController extends EntityDefaultViewsController {

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function views_data() {
    $data = parent::views_data();

    $data['compost_variable']['value']['field']['handler'] = 'views_handler_field_variable';
    $data['compost_variable']['group']['filter']['handler'] = 'views_handler_filter_group';

    return $data;
  }
}


class CompostSiteStatusEntity extends Entity {

  /**
   * Check if the status is equal to another site status.
   */
  public function statusEquals($other) {
    return $this->nid == $other->nid
      && $this->status == $other->status
      && $this->last_message == $other->last_message;
  }
}


/**
 * Implements hook_entity_info().
 */
function compost_entity_info() {
  return array(
    'compost_site_status' => array(
      'label' => t('Site status'),
      'plural label' => t('Site status'),
      'controller class' => 'EntityAPIController',
      'entity class' => 'CompostSiteStatusEntity',
      'base table' => 'compost_site_status',
      'entity keys' => array(
        'id' => 'nid',
      ),
      'views controller class' => 'CompostSiteStatusViewsController',
      'module' => 'compost',
    ),

    'compost_group' => array(
      'label' => t('Site data group'),
      'plural label' => t('Site data groups'),
      'controller class' => 'EntityAPIController',
      'entity class' => 'Entity',
      'base table' => 'compost_group',
      'entity keys' => array(
        'id' => 'gid',
      ),
      'module' => 'compost',
    ),

    'compost_alert' => array(
      'label' => t('Site alert'),
      'plural label' => t('Site alerts'),
      'controller class' => 'EntityAPIController',
      'entity class' => 'Entity',
      'base table' => 'compost_alert',
      'entity keys' => array(
        'id' => 'alid',
      ),
      'views controller class' => 'CompostAlertViewsController',
      'module' => 'compost',
    ),

    'compost_command' => array(
      'label' => t('Site command'),
      'plural label' => t('Site commands'),
      'controller class' => 'EntityAPIController',
      'entity class' => 'Entity',
      'base table' => 'compost_command',
      'entity keys' => array(
        'id' => 'cmdid',
      ),
      'views controller class' => 'CompostCommandViewsController',
      'module' => 'compost',
    ),

    'compost_variable' => array(
      'label' => t('Site variable'),
      'plural label' => t('Site variables'),
      'controller class' => 'EntityAPIController',
      'entity class' => 'Entity',
      'base table' => 'compost_variable',
      'entity keys' => array(
        'id' => 'varid',
      ),
      'views controller class' => 'CompostVariableViewsController',
      'module' => 'compost',
    ),
  );
}


/**
 * Implements hook_entity_property_info().
 */
function compost_entity_property_info() {
  return array(
    'compost_site_status' => array(
      'properties' => array(
        'nid' => array(
          'label' => t('Node'),
          'description' => t('The site node.'),
          'type' => 'node',
          'schema field' => 'nid',
        ),
        'status' => array(
          'label' => t('Status'),
          'description' => t('The site status.'),
          'type' => 'integer',
          'schema field' => 'status',
          'options list' => 'compost_status_map',
          'setter callback' => 'entity_property_verbatim_set',
        ),
        'last_message' => array(
          'label' => t('Last message'),
          'description' => t('The last alert message.'),
          'type' => 'text',
          'schema field' => 'last_message',
        ),
        'changed' => array(
          'label' => t('Status date/time'),
          'description' => t('The site status date/time.'),
          'type' => 'date',
          'schema field' => 'changed',
        ),
        'updated' => array(
          'label' => t('Update date/time'),
          'description' => t('The site update date/time.'),
          'type' => 'date',
          'schema field' => 'updated',
        ),
        'overriden' => array(
          'label' => t('Overriden'),
          'description' => t('The overriden flag.'),
          'type' => 'boolean',
          'schema field' => 'overriden',
        ),
      ),
    ),

    'compost_group' => array(
      'properties' => array(
        'gid' => array(
          'label' => t('Group ID'),
          'description' => t('The data group ID.'),
          'type' => 'integer',
          'schema field' => 'gid',
        ),
        'name' => array(
          'label' => t('Name'),
          'description' => t('The data group node.'),
          'type' => 'text',
          'schema field' => 'name',
        ),
      ),
    ),

    'compost_alert' => array(
      'properties' => array(
        'alid' => array(
          'label' => t('Alert ID'),
          'description' => t('The alert ID.'),
          'type' => 'integer',
          'schema field' => 'alid',
        ),
        'nid' => array(
          'label' => t('Node'),
          'description' => t('The site node.'),
          'type' => 'node',
          'schema field' => 'nid',
          'setter callback' => 'entity_property_verbatim_set',
        ),
        'akey' => array(
          'label' => t('Key'),
          'description' => t('The alert key.'),
          'type' => 'text',
          'schema field' => 'akey',
        ),
        'name' => array(
          'label' => t('Name'),
          'description' => t('The alert name.'),
          'type' => 'text',
          'schema field' => 'name',
        ),
        'description' => array(
          'label' => t('Description'),
          'description' => t('The alert description.'),
          'type' => 'text',
          'schema field' => 'description',
        ),
        'status' => array(
          'label' => t('Status'),
          'description' => t('The alert status.'),
          'type' => 'integer',
          'schema field' => 'status',
          'options list' => 'compost_status_map',
          'setter callback' => 'entity_property_verbatim_set',
        ),
        'changed' => array(
          'label' => t('Status date/time'),
          'description' => t('The status date/time.'),
          'type' => 'date',
          'schema field' => 'changed',
        ),
        'ignored' => array(
          'label' => t('Ignored'),
          'description' => t('The alert ignored flag.'),
          'type' => 'boolean',
          'schema field' => 'ignored',
        ),
        'group' => array(
          'label' => t('Group'),
          'description' => t('The alert data group.'),
          'type' => 'compost_group',
          'schema field' => 'gid',
          'setter callback' => 'entity_property_verbatim_set',
        ),
      ),
    ),

    'compost_command' => array(
      'properties' => array(
        'cmdid' => array(
          'label' => t('Command ID'),
          'description' => t('The command ID.'),
          'type' => 'integer',
          'schema field' => 'cmdid',
        ),
        'nid' => array(
          'label' => t('Node'),
          'description' => t('The site node.'),
          'type' => 'node',
          'schema field' => 'nid',
          'setter callback' => 'entity_property_verbatim_set',
        ),
        'ckey' => array(
          'label' => t('Key'),
          'description' => t('The command key.'),
          'type' => 'text',
          'schema field' => 'ckey',
        ),
        'name' => array(
          'label' => t('Name'),
          'description' => t('The command name.'),
          'type' => 'text',
          'schema field' => 'name',
        ),
        'description' => array(
          'label' => t('Description'),
          'description' => t('The command description.'),
          'type' => 'text',
          'schema field' => 'description',
        ),
        'group' => array(
          'label' => t('Group'),
          'description' => t('The alert data group.'),
          'type' => 'compost_group',
          'schema field' => 'gid',
          'setter callback' => 'entity_property_verbatim_set',
        ),
      ),
    ),

    'compost_variable' => array(
      'properties' => array(
        'varid' => array(
          'label' => t('Variable ID'),
          'description' => t('The variable ID.'),
          'type' => 'integer',
          'schema field' => 'varid',
        ),
        'nid' => array(
          'label' => t('Node'),
          'description' => t('The site node.'),
          'type' => 'node',
          'schema field' => 'nid',
          'setter callback' => 'entity_property_verbatim_set',
        ),
        'vkey' => array(
          'label' => t('Key'),
          'description' => t('The variable key.'),
          'type' => 'text',
          'schema field' => 'vkey',
        ),
        'name' => array(
          'label' => t('Name'),
          'description' => t('The variable name.'),
          'type' => 'text',
          'schema field' => 'name',
        ),
        'dtype' => array(
          'label' => t('Data type'),
          'description' => t('The variable internal data type.'),
          'type' => 'text',
          'schema field' => 'dtype',
          'options list' => 'compost_dtype_map',
          'setter callback' => 'entity_property_verbatim_set',
        ),
        'value' => array(
          'label' => t('Value'),
          'description' => t('The raw variable value.'),
          'type' => 'text',
          'schema field' => 'value',
        ),
        'value_i' => array(
          'label' => t('Value (integer)'),
          'description' => t('The value of integer variables.'),
          'type' => 'integer',
          'schema field' => 'value_i',
        ),
        'value_f' => array(
          'label' => t('Value (float)'),
          'description' => t('The value of float variables.'),
          'type' => 'integer',
          'schema field' => 'value_f',
        ),
        'value_b' => array(
          'label' => t('Value (boolean)'),
          'description' => t('The value for boolean variables.'),
          'type' => 'boolean',
          'schema field' => 'value_b',
        ),
        'value_t' => array(
          'label' => t('Value (date/time)'),
          'description' => t('The value for date/time variables.'),
          'type' => 'date',
          'schema field' => 'value_t',
        ),
        'value_v_core' => array(
          'label' => t('Value (version - Drupal core)'),
          'description' => t('The Drupal core value of version variables.'),
          'type' => 'integer',
          'schema field' => 'value_v_core',
        ),
        'value_v_major' => array(
          'label' => t('Value (version - major version)'),
          'description' => t('The major version of version variables.'),
          'type' => 'integer',
          'schema field' => 'value_v_major',
        ),
        'value_v_minor' => array(
          'label' => t('Value (version - minor version)'),
          'description' => t('The minor version of version variables.'),
          'type' => 'integer',
          'schema field' => 'value_v_minor',
        ),
        'value_v_patch' => array(
          'label' => t('Value (version - patch level)'),
          'description' => t('The patch level of version variables.'),
          'type' => 'integer',
          'schema field' => 'value_v_patch',
        ),
        'value_v_tag' => array(
          'label' => t('Value (version - release tag)'),
          'description' => t('The release tag of version variables.'),
          'type' => 'text',
          'schema field' => 'value_v_tag',
          'options list' => 'compost_version_tag_map',
          'setter callback' => 'entity_property_verbatim_set',
        ),
        'value_v_tagn' => array(
          'label' => t('Value (version - release tag number)'),
          'description' => t('The release tag number of version variables.'),
          'type' => 'text',
          'schema field' => 'value_v_tagn',
        ),
        'changed' => array(
          'label' => t('Variable date/time'),
          'description' => t('The date/time when the variable value was fetched.'),
          'type' => 'date',
          'schema field' => 'changed',
        ),
        'group' => array(
          'label' => t('Group'),
          'description' => t('The alert data group.'),
          'type' => 'compost_group',
          'schema field' => 'gid',
          'setter callback' => 'entity_property_verbatim_set',
        ),
      ),
    ),
  );
}
