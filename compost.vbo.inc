<?php
/**
 * @file
 * Views bulk operations integration functions.
 */


/**
 * Return the "compost_check_cron" (VBO) view.
 */
function _compost_get_vbo_compost_check_cron() {
  $view = new view();
  $view->name = 'compost_check_cron';
  $view->description = 'Check sites that did not run cron recently and allows you to selectively run cron on them.';
  $view->tag = 'command post';
  $view->base_table = 'node';
  $view->human_name = 'Check cron on Drupal Sites';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = TRUE;

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Check cron';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'run compost commands';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Relationship: Content: Site variable */
  $handler->display->display_options['relationships']['compost_variable']['id'] = 'compost_variable';
  $handler->display->display_options['relationships']['compost_variable']['table'] = 'node';
  $handler->display->display_options['relationships']['compost_variable']['field'] = 'compost_variable';
  /* Field: Bulk operations: Content */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'node';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['label'] = '';
  $handler->display->display_options['fields']['views_bulk_operations']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '1';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'rules_component::compost_run_cron' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 1,
      'label' => 'Run cron',
    ),
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Site variable: Value (date/time) */
  $handler->display->display_options['fields']['value_t']['id'] = 'value_t';
  $handler->display->display_options['fields']['value_t']['table'] = 'compost_variable';
  $handler->display->display_options['fields']['value_t']['field'] = 'value_t';
  $handler->display->display_options['fields']['value_t']['relationship'] = 'compost_variable';
  $handler->display->display_options['fields']['value_t']['label'] = 'Last run';
  $handler->display->display_options['fields']['value_t']['date_format'] = 'time ago';
  $handler->display->display_options['fields']['value_t']['second_date_format'] = 'long';
  /* Sort criterion: Site variable: Value (date/time) */
  $handler->display->display_options['sorts']['value_t']['id'] = 'value_t';
  $handler->display->display_options['sorts']['value_t']['table'] = 'compost_variable';
  $handler->display->display_options['sorts']['value_t']['field'] = 'value_t';
  $handler->display->display_options['sorts']['value_t']['relationship'] = 'compost_variable';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Site URL (compost_url:url) */
  $handler->display->display_options['filters']['compost_url_url']['id'] = 'compost_url_url';
  $handler->display->display_options['filters']['compost_url_url']['table'] = 'field_data_compost_url';
  $handler->display->display_options['filters']['compost_url_url']['field'] = 'compost_url_url';
  $handler->display->display_options['filters']['compost_url_url']['operator'] = 'not empty';
  $handler->display->display_options['filters']['compost_url_url']['group'] = 1;
  /* Filter criterion: Site variable: Key */
  $handler->display->display_options['filters']['vkey']['id'] = 'vkey';
  $handler->display->display_options['filters']['vkey']['table'] = 'compost_variable';
  $handler->display->display_options['filters']['vkey']['field'] = 'vkey';
  $handler->display->display_options['filters']['vkey']['relationship'] = 'compost_variable';
  $handler->display->display_options['filters']['vkey']['value'] = 'compostagent.core.cron_last';
  $handler->display->display_options['filters']['vkey']['group'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'compost/check_cron';

  return $view;
}


/**
 * Returns all VBO views.
 */
function _compost_get_vbo_views() {
  $views = array();

  $view = _compost_get_vbo_compost_check_cron();
  $views[$view->name] = $view;

  return $views;
}
