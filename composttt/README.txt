Summary
=======

The Command Post test tool module enables a developer debug the
Command Post protocol by generating and sending arbitrary requests to
a Command Post agent.


Requirements
============

The Command Post test tool requires the Devel module
(http://drupal.org/project/devel).


Installation
============

Being a developer module, it is hidden by default in the Drupal module
list. So, to enable the tool you must use drush.

Use the following command to enable the module:

  $ drush en composttt


Usage
=====

Go to devel/composttt on your Drupal site to access the tool
interface. To test an agent, you basically select the site, the
requests, and any additional info.

The *Command* field is used only by the "run" request. Use this field
to input the command ID of the command you want to run on the agent.
