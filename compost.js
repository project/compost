/**
 * @file
 * Javascript functions and behaviors for the Command Post module.
 */
(function ($) {

  function attach_behavior(context, settings) {
    $("input#edit-compost-key-und-0-value,input#edit-compost-key", context)
      .once('compost-key')
      .each(function() {
        var $this = $(this);

        if ($this.val() != '') {
          var $fake_key = $('<input type="password">')
              .attr('disabled', 'disabled')
              .attr('size', $this.attr('size'))
              .val('?'.repeat(32));
          var $show_link = $('<a>')
              .attr('href', '#')
          .text(Drupal.t('Edit'))
              .click(function() {
                // Hide link.
                $(this).hide();
                $fake_key.fadeOut(function () {
                  // Fade input field in.
                  $this.fadeIn();
                });
                return false;
              });
          $this
            .after($show_link)
            .after($fake_key);
        }
        else {
          $this.show();
        }
      });
  }

  Drupal.behaviors.compostModule = {
    attach: attach_behavior,
  };

})(jQuery);
