<?php
/**
 * @file
 * Cryptographic classes using OpenSSL.
 */


/**
 * The OpenSSL-based AES cipher.
 */
class CompostOpenSSLAESCipher extends CompostCipher {
  protected $bits;
  protected $mode;

  /**
   * Constructs the object.
   */
  public function __construct($bits, $mode) {
    assert('$bits == 128');
    assert('$mode == "cbc"');
    $this->bits = $bits;
    $this->mode = $mode;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'aes-' . $this->bits . '-' . $this->mode;
  }

  /**
   * Derive a key from a password.
   */
  protected function deriveKey($password) {
    return substr(hash('sha1', $password, TRUE), 0, 16);
  }

  /**
   * {@inheritdoc}
   */
  public function encrypt($data, $password) {
    $key = $this->deriveKey($password);

    $cipher = $this->getName();

    $iv_size = openssl_cipher_iv_length($cipher);
    $iv = openssl_random_pseudo_bytes($iv_size);

    return $iv . openssl_encrypt($data, $cipher, $key, OPENSSL_RAW_DATA, $iv);
  }

  /**
   * {@inheritdoc}
   */
  public function decrypt($data, $password) {
    $key = $this->deriveKey($password);

    $cipher = $this->getName();
    $iv_size = openssl_cipher_iv_length($cipher);

    $iv = substr($data, 0, $iv_size);
    $data = substr($data, $iv_size);

    return openssl_decrypt($data, $cipher, $key, OPENSSL_RAW_DATA, $iv);
  }
}


/**
 * The OpenSSL-based cipher factory.
 */
class CompostOpenSSLCipherFactory extends CompostCipherFactory {

  /**
   * {@inheritdoc}
   */
  public function newCipher($algo) {
    if ($algo == 'aes-128-cbc') {
      return new CompostOpenSSLAESCipher(128, 'cbc');
    }

    throw new CompostProtocolException("Unsupported openssl cipher algorithm: $algo");
  }
}
