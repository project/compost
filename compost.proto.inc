<?php
/**
 * @file
 * Classes and functions related to the Command Post protocol.
 */

/**
 * Abstract base class to implement ciphers.
 */
abstract class CompostCipher {
  /**
   * Returns the cipher name (e.g., "aes-128-cbc").
   */
  abstract public function getName();

  /**
   * Encrypts data using a password.
   */
  abstract public function encrypt($data, $password);

  /**
   * Decrypts data using a password.
   */
  abstract public function decrypt($data, $password);
}


/**
 * Abstract base class to implement cipher factories.
 */
abstract class CompostCipherFactory {
  /**
   * Create a new cipher object.
   *
   * @param string $algo
   *   The algorithm name (e.g., 'aes-128-cbc').
   *
   * @return CompostCipher
   *   A cipher object.
   *
   * @throws CompostProtocolException
   *   If the supplied cipher name is not supported.
   */
  abstract public function newCipher($algo);
}


/**
 * Our generic exception.
 */
class CompostProtocolException extends Exception {}


/**
 * Represents a Command Post message.
 */
class CompostMessage {
  protected $data = array();

  /**
   * Constructs the object.
   */
  public function __construct() {
    $this->data['time'] = time();
  }

  /**
   * Add a request to the message.
   */
  public function addRequest($request, $args = NULL) {
    if (!isset($this->data['request'])) {
      $this->data['request'] = array();
    }
    $this->data['request'][$request] = $args;
  }

  /**
   * Returns a specific response from the message.
   */
  public function getResponse($request) {
    return (isset($this->data['response'][$request]) ?
            $this->data['response'][$request] :
            NULL);
  }

  /**
   * Returns all user messages (e.g., set with drupal_set_message()).
   */
  public function getMessages() {
    return (isset($this->data['messages']) ? $this->data['messages'] : array());
  }

  /**
   * Serialize our message.
   */
  public function serialize() {
    return drupal_json_encode($this->data);
  }

  /**
   * Unserialize our message.
   */
  public function unserialize($string) {
    $data = drupal_json_decode($string);
    if (!isset($data['time'])) {
      throw new CompostProtocolException('Invalid message');
    }
    $this->data = $data;
  }

  /**
   * Returns all message data.
   */
  public function getData() {
    return $this->data;
  }
}


/**
 * Represents an HTTP request that (possibly) contains a Command Post message.
 */
class CompostRequest {
  protected $agent_key;
  protected $manager_key;
  protected $payload;
  protected $cipher;
  protected $extra;

  /**
   * Constructs the object.
   */
  public function __construct($agent_key, $manager_key, $extra = array()) {
    $this->agent_key = $agent_key;
    $this->manager_key = $manager_key;
    $this->extra = $extra;
  }

  /**
   * Sets the cipher object that should be used to encrypt the communications.
   */
  public function setCipher(CompostCipher $cipher) {
    $this->cipher = $cipher;
  }

  /**
   * Sets the request payload.
   */
  public function setPayload($payload) {
    $this->payload = $payload;
  }

  /**
   * Returns the request payload.
   */
  public function getPayload() {
    return $this->payload;
  }

  /**
   * Returns the request body to be used for this request.
   */
  public function getBody() {

    if ($this->cipher) {
      $password = $this->agent_key . $this->manager_key;
      $payload = base64_encode($this->cipher->encrypt($this->payload,
                                                      $password));
    }
    else {
      $payload = $this->payload;
    }

    $body = 'msg=' . urlencode($payload)
      . '&macalgo=hmac-sha256&mac='
      . urlencode(base64_encode(hash_hmac('sha256',
                                          $payload,
                                          $this->manager_key,
                                          TRUE)));

    if ($this->cipher) {
      $body .= '&cipher=' . $this->cipher->getName();
    }

    if ($this->extra) {
      $body .= '&' . http_build_query($this->extra);
    }

    return $body;
  }

  /**
   * Return the HTTP method to be used for this request.
   */
  public function getMethod() {
    return 'POST';
  }

  /**
   * Returns an array with additional headers to be used for this request.
   */
  public function getHeaders() {
    return array(
      'Content-Type' => 'application/x-www-form-urlencoded',
    );
  }
}


/**
 * The Command Post service class.
 */
class CompostProtocolService {
  protected $cipher_factory;
  protected $manager_key;


  /**
   * Constructs the object.
   */
  public function __construct($manager_key, CompostCipherFactory $cipher_factory) {
    $this->manager_key = $manager_key;
    $this->cipher_factory = $cipher_factory;
  }

  /**
   * Sets the manager key.
   */
  public function setManagerKey($key) {
    $this->manager_key = $key;
  }

  /**
   * Send a message.
   *
   * @param node $node
   *   The site node that the message should be sent to.
   *
   * @param CompostMessage $msg
   *   The message to be sent.
   *
   * @param string $endpoint
   *   The optional endpoint the request should be sent to.
   *
   * @param array $extra
   *   Extra parameters to be added to the request.
   *
   * @returns CompostMessage
   *   The reply message.
   *
   * @throws CompostProtocolException
   *   If any problem is encountered.
   */
  public function send($node, CompostMessage $msg, $endpoint = NULL, $extra = array()) {

    $url = $node->compost_url[LANGUAGE_NONE][0]['url'];

    if ($node->compost_clean_urls[LANGUAGE_NONE][0]['value']) {
      $url .= "/compostagent/request";
    }
    else {
      $url .= "/?q=compostagent/request";
    }

    if ($endpoint) {
      $url .= "/$endpoint";
    }

    $req = new CompostRequest($node->compost_key[LANGUAGE_NONE][0]['value'],
                              $this->manager_key, $extra);

    $req->setPayload($msg->serialize());

    $encrypt = (substr($url, 0, 8) != 'https://');
    if ($encrypt) {
      $req->setCipher($this->cipher_factory->newCipher('aes-128-cbc'));
    }

    $res = drupal_http_request($url,
                               array(
                                 'method' => $req->getMethod(),
                                 'headers' => $req->getHeaders(),
                                 'data' => $req->getBody(),
                                 'timeout' => 60,
                               ));

    if ($res->code == 404) {
      throw new CompostProtocolException(t('Agent not enabled'));
    }
    elseif ($res->code == HTTP_REQUEST_TIMEOUT) {
      throw new CompostProtocolException(t('Request timed out'));
    }
    elseif ($res->code != 200) {
      throw new CompostProtocolException(t('HTTP @code @message',
                                        array(
                                          '@code' => $res->code,
                                          '@message' => $res->error,
                                        )));
    }

    $agent_key = $node->compost_key[LANGUAGE_NONE][0]['value'];

    $resp = new CompostResponse($agent_key, $res);

    if (!$resp->isAuthentic()) {
      throw new CompostProtocolException(t('Cannot assert message authenticity. Wrong keys?'));
    }

    $body = $resp->getBody();

    $cipher_algo = $resp->getCipher();
    if ($cipher_algo) {
      $cipher = $this->cipher_factory->newCipher($cipher_algo);
      $body = $cipher->decrypt($body, $agent_key . $this->manager_key);
    }
    elseif ($encrypt) {
      throw new CompostProtocolException(t('Protocol violation: plaintext response'));
    }

    $reply = new CompostMessage();
    $reply->unserialize($body);

    return $reply;
  }
}


/**
 * Represents a Command Post response received from an agent.
 */
class CompostResponse {
  protected $agent_key;
  protected $cipher_algo;
  protected $mac_algo;
  protected $mac;
  protected $body;

  /**
   * Constructs the object.
   */
  public function __construct($agent_key, $res) {
    if (empty($res->headers['x-compost-mac'])) {
      throw new CompostProtocolException('Invalid response from agent');
    }

    $i = explode(' ', $res->headers['x-compost-mac']);
    if (count($i) != 2) {
      throw new CompostProtocolException('Malformed response from agent');
    }

    if ($i[0] != 'hmac-sha256') {
      throw new CompostProtocolException("Unsuported MAC algorithm: $i[0]");
    }

    $this->mac_algo = $i[0];
    $this->mac = base64_decode($i[1]);

    $this->body = $res->data;

    if (!empty($res->headers['x-compost-cipher'])) {
      $this->cipher_algo = $res->headers['x-compost-cipher'];
    }

    $this->agent_key = $agent_key;
  }

  /**
   * Check message authenticity.
   *
   * @return bool
   *   TRUE if the message is authentic (e.g., signed by the provided
   *   key), FALSE if not.
   */
  public function isAuthentic() {
    return compost_hash_equals(hash_hmac('sha256', $this->body,
                                         $this->agent_key, TRUE),
                               $this->mac);
  }

  /**
   * Returns the cipher algorithm used for this response.
   */
  public function getCipher() {
    return $this->cipher_algo;
  }

  /**
   * Get the (possibly encrypted) response body.
   */
  public function getBody() {
    return $this->body;
  }
}
