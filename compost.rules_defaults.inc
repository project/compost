<?php
/**
 * @file
 * Default rules for the Command Post module.
 */


/**
 * Implements hook_default_rules_configuration().
 */
function compost_default_rules_configuration() {

  $rules = array(

    // Codesniffer is not able to tell that we are inside a multiline
    // JSON string, all will throw a lot of errors, so we temporarily
    // disable error checking.
    // @codingStandardsIgnoreStart
    'compost_rule_reset_site_logo' => '
{ "compost_rule_reset_site_logo" : {
    "LABEL" : "Reset site logo after update",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "Command Post" ],
    "REQUIRES" : [ "rules", "compost" ],
    "ON" : { "compost_after_update" : [] },
    "IF" : [
      { "entity_has_field" : { "entity" : [ "node" ], "field" : "compost_logo" } },
      { "data_is_empty" : { "data" : [ "node:compost-logo" ] } }
    ],
    "DO" : [
      { "component_compost_reset_site_logo" : { "node" : [ "node" ] } }
    ]
  }
}
',

    'compost_notify' => '
{ "rules_compost_notify" : {
    "LABEL" : "Notify on Command Post notice\/fail status",
    "PLUGIN" : "reaction rule",
    "ACTIVE" : false,
    "OWNER" : "rules",
    "TAGS" : [ "Command Post" ],
    "REQUIRES" : [ "rules", "compost" ],
    "ON" : { "compost_site_status_change" : [] },
    "IF" : [
      { "node_is_published" : { "node" : [ "node" ] } },
      { "data_is" : {
          "data" : [ "new-status:status" ],
          "op" : "\u003E",
          "value" : [ "old-status:status" ]
        }
      }
    ],
    "DO" : [
      { "mail" : {
          "to" : "[site:mail]",
          "subject" : "[[site:name]]: [node:title] [new-status:status-name]",
          "message" : "Dear Webmaster,\r\n\r\nThis a status change notification from [site:name]:\r\n\r\n   Site: [node:title]\r\n   Status change: [old-status:status-name] -\u003E [new-status:status-name]\r\n   Message: [new-status:last_message]\r\n\r\nFor more information, copy and paste the address below on your web browser:\r\n\r\n   [node:url]\r\n\r\n-- \r\n[site:name]\r\nCommand Post Team\r\n",
          "language" : [ "" ]
        }
      }
    ]
  }
}
',

    'compost_notify_ok' => '
{ "rules_compost_notify_ok" : {
    "LABEL" : "Notify on Command Post Ok status",
    "PLUGIN" : "reaction rule",
    "ACTIVE" : false,
    "OWNER" : "rules",
    "TAGS" : [ "Command Post" ],
    "REQUIRES" : [ "rules", "compost" ],
    "ON" : { "compost_site_status_change" : [] },
    "IF" : [
      { "node_is_published" : { "node" : [ "node" ] } },
      { "data_is" : { "data" : [ "new-status:status" ], "value" : "0" } }
    ],
    "DO" : [
      { "mail" : {
          "to" : "[site:mail]",
          "subject" : "[[site:name]]: [node:title] [new-status:status-name]",
          "message" : "Dear Webmaster,\r\n\r\nThis a status change notification from [site:name]:\r\n\r\n   Site: [node:title]\r\n   Status change: [old-status:status-name] -\u003E [new-status:status-name]\r\n\r\nFor more information, copy and paste the address below on your web browser:\r\n\r\n   [node:url]\r\n\r\n-- \r\n[site:name]\r\nCommand Post Team\r\n",
          "language" : [ "" ]
        }
      }
    ]
  }
}',

    'compost_get_site_drupal_version' => '
{ "rules_compost_get_site_drupal_version" : {
    "LABEL" : "Get a site Drupal version",
    "PLUGIN" : "action set",
    "OWNER" : "rules",
    "TAGS" : [ "Command Post" ],
    "REQUIRES" : [ "compost", "rules" ],
    "USES VARIABLES" : {
      "node" : { "label" : "Node", "type" : "node" },
      "drupal_version" : {
        "label" : "Drupal version",
        "type" : "compost_variable",
        "parameter" : false
      }
    },
    "ACTION SET" : [
      { "compost_fetch_variable" : {
          "USING" : { "node" : [ "node" ], "vkey" : "compostagent.core.version" },
          "PROVIDE" : { "variable_fetched" : { "drupal_version_fetched" : "Fetched Drupal version" } }
        }
      },
      { "data_set" : { "data" : [ "drupal-version" ], "value" : [ "drupal-version-fetched" ] } }
    ],
    "PROVIDES VARIABLES" : [ "drupal_version" ]
  }
}',

    'compost_get_drupal_version_insecure_status' => '
{ "rules_compost_get_drupal_version_insecure_status" : {
    "LABEL" : "Get Drupal version insecure status",
    "PLUGIN" : "rule",
    "OWNER" : "rules",
    "TAGS" : [ "Command Post" ],
    "REQUIRES" : [ "rules" ],
    "USES VARIABLES" : {
      "version" : { "label" : "Version", "type" : "compost_variable" },
      "insecure" : { "label" : "Is insecure", "type" : "boolean", "parameter" : false }
    },
    "IF" : [
      { "OR" : [
          { "AND" : [
              { "data_is" : { "data" : [ "version:value-v-major" ], "value" : "6" } },
              { "data_is" : { "data" : [ "version:value-v-minor" ], "op" : "\u003C", "value" : "34" } }
            ]
          },
          { "AND" : [
              { "data_is" : { "data" : [ "version:value-v-major" ], "value" : "7" } },
              { "data_is" : { "data" : [ "version:value-v-minor" ], "op" : "\u003C", "value" : "34" } }
            ]
          }
        ]
      }
    ],
    "DO" : [ { "data_set" : { "data" : [ "insecure" ], "value" : "1" } } ],
    "PROVIDES VARIABLES" : [ "insecure" ]
  }
}',

    'compost_check_drupal_version' => '
{ "compost_check_drupal_version" : {
    "LABEL" : "Check Drupal version",
    "PLUGIN" : "reaction rule",
    "ACTIVE" : false,
    "OWNER" : "rules",
    "TAGS" : [ "Command Post" ],
    "REQUIRES" : [ "rules", "compost" ],
    "ON" : { "compost_after_update" : [] },
    "DO" : [
      { "component_compost_get_site_drupal_version" : {
          "USING" : { "node" : [ "node" ] },
          "PROVIDE" : { "drupal_version" : { "drupal_version" : "Drupal version" } }
        }
      },
      { "component_compost_get_drupal_version_insecure_status" : {
          "USING" : { "version" : [ "drupal-version" ] },
          "PROVIDE" : { "insecure" : { "insecure" : "Is insecure" } }
        }
      },
      { "component_compost_cond_generate_fail_alert" : {
          "condition" : [ "insecure" ],
          "node" : [ "node" ],
          "key" : "insecure_drupal_version",
          "name" : "Insecure Drupal version",
          "description" : "This site is using Drupal [drupal-version:value], which is an insecure version. Visit \u003Ca href=\u0022https:\/\/www.drupal.org\/security\u0022\u003E\u003Cem\u003ESecurity advisories\u003C/em\u003E on Drupal.org\u003C\/a\u003E for more information about insecure Drupal versions."
        }
      }
    ]
  }
}',

    'compost_cond_generate_fail_alert' => '
{ "compost_cond_generate_fail_alert" : {
    "LABEL" : "Conditionally generate a fail alert",
    "PLUGIN" : "rule",
    "OWNER" : "rules",
    "TAGS" : [ "Command Post" ],
    "REQUIRES" : [ "rules", "compost" ],
    "USES VARIABLES" : {
      "condition" : { "label" : "Condition", "type" : "boolean" },
      "node" : { "label" : "Node", "type" : "node" },
      "key" : { "label" : "Key ", "type" : "token" },
      "name" : { "label" : "Name", "type" : "text" },
      "description" : { "label" : "Description", "type" : "text" }
    },
    "IF" : [ { "data_is" : { "data" : [ "condition" ], "value" : "1" } } ],
    "DO" : [
      { "compost_create_alert" : {
          "USING" : {
            "node" : [ "node" ],
            "key" : [ "key" ],
            "name" : [ "name" ],
            "description" : [ "description" ],
            "status" : "20"
          },
          "PROVIDE" : { "alert_created" : { "fail_alert_created" : "Created fail alert" } }
        }
      }
    ]
  }
}',

    'compost_cond_generate_notice_alert' => '
{ "rules_compost_cond_generate_notice_alert" : {
    "LABEL" : "Conditionally generate a notice alert",
    "PLUGIN" : "rule",
    "OWNER" : "rules",
    "TAGS" : [ "Command Post" ],
    "REQUIRES" : [ "rules", "compost" ],
    "USES VARIABLES" : {
      "condition" : { "label" : "Condition", "type" : "boolean" },
      "node" : { "label" : "Node", "type" : "node" },
      "key" : { "label" : "Key ", "type" : "token" },
      "name" : { "label" : "Name", "type" : "text" },
      "description" : { "label" : "Description", "type" : "text" }
    },
    "IF" : [ { "data_is" : { "data" : [ "condition" ], "value" : "1" } } ],
    "DO" : [
      { "compost_create_alert" : {
          "USING" : {
            "node" : [ "node" ],
            "key" : [ "key" ],
            "name" : [ "name" ],
            "description" : [ "description" ],
            "status" : "10"
          },
          "PROVIDE" : { "alert_created" : { "notice_alert_created" : "Created notice alert" } }
        }
      }
    ]
  }
}',

    'compost_check_maintenance_mode' => '
{ "rules_compost_check_maintenance_mode" : {
    "LABEL" : "Check maintenance mode",
    "PLUGIN" : "reaction rule",
    "ACTIVE" : false,
    "OWNER" : "rules",
    "TAGS" : [ "Command Post" ],
    "REQUIRES" : [ "compost", "rules" ],
    "ON" : { "compost_after_update" : [] },
    "DO" : [
      { "compost_fetch_variable" : {
          "USING" : { "node" : [ "node" ], "vkey" : "compostagent.core.maintenance" },
          "PROVIDE" : { "variable_fetched" : { "maintenance_variable" : "Mainenance variable" } }
        }
      },
      { "component_compost_cond_generate_notice_alert" : {
          "condition" : [ "maintenance-variable:value-b" ],
          "node" : [ "node" ],
          "key" : "maintenance",
          "name" : "Maintenance mode",
          "description" : "Site is in maintenance mode."
        }
      }
    ]
  }
}',

    'compost_run_command' => '
{ "compost_run_command" : {
    "LABEL" : "Run a remote command",
    "PLUGIN" : "action set",
    "OWNER" : "rules",
    "TAGS" : [ "Command Post" ],
    "REQUIRES" : [ "compost" ],
    "USES VARIABLES" : {
      "node" : { "label" : "Site node", "type" : "node" },
      "ckey" : { "label" : "Command key", "type" : "text" }
    },
    "ACTION SET" : [
      { "compost_fetch_command" : {
          "USING" : { "node" : [ "node" ], "ckey" : [ "ckey" ] },
          "PROVIDE" : { "command_fetched" : { "command_fetched" : "Fetched command" } }
        }
      },
      { "compost_run_command" : { "node" : [ "node" ], "command" : [ "command-fetched" ] } }
    ]
  }
}',

    'compost_run_cron' => '
{ "compost_run_cron" : {
    "LABEL" : "Run Cron on a site",
    "PLUGIN" : "action set",
    "OWNER" : "rules",
    "TAGS" : [ "Command Post" ],
    "REQUIRES" : [ "rules", "compost" ],
    "USES VARIABLES" : { "node" : { "label" : "Site node", "type" : "node" } },
    "ACTION SET" : [
      { "component_compost_run_command" : { "node" : [ "node" ], "ckey" : "compostagent.core.run_cron" } },
      { "compost_update_node" : { "node" : [ "node" ] } }
    ]
  }
}',

    'compost_reset_site_logo' => '
{ "compost_reset_site_logo" : {
    "LABEL" : "Reset site logo",
    "PLUGIN" : "rule set",
    "OWNER" : "rules",
    "TAGS" : [ "Command Post" ],
    "REQUIRES" : [ "compost", "rules" ],
    "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
    "RULES" : [
      { "RULE" : {
          "PROVIDE" : {
            "variable_fetched" : { "logo_url_variable" : "Logo URL variable" },
            "variable_added" : { "logo_url" : "Logo URL" }
          },
          "DO" : [
            { "compost_fetch_variable" : {
                "USING" : { "node" : [ "node" ], "vkey" : "compostagent.core.logo" },
                "PROVIDE" : { "variable_fetched" : { "logo_url_variable" : "Logo URL variable" } }
              }
            },
            { "variable_add" : {
                "USING" : { "type" : "text", "value" : [ "logo-url-variable:value" ] },
                "PROVIDE" : { "variable_added" : { "logo_url" : "Logo URL" } }
              }
            }
          ],
          "LABEL" : "Load logo URL"
        }
      },
      { "RULE" : {
          "IF" : [
            { "NOT text_matches" : {
                "text" : [ "logo-url" ],
                "match" : "^https?:\/\/",
                "operation" : "regex"
              }
            }
          ],
          "DO" : [
            { "data_set" : {
                "data" : [ "logo-url" ],
                "value" : "[node:compost-url:url]\/[logo-url:value]"
              }
            }
          ],
          "LABEL" : "Normalize logo URL"
        }
      },
      { "RULE" : {
          "IF" : [
            { "entity_has_field" : { "entity" : [ "node" ], "field" : "compost_logo" } },
            { "NOT data_is" : { "data" : [ "logo-url" ], "value" : "[node:compost-logo:url]" } }
          ],
          "DO" : [
            { "data_set" : { "data" : [ "node:compost-logo:url" ], "value" : "[logo-url:value]" } },
            { "drupal_message" : { "message" : "\u003Cem class=\u0022placeholder\u0022\u003E[node:title]\u003C\/em\u003E logo has been updated." } }
          ],
          "LABEL" : "Reset site URL"
        }
      }
    ]
  }
}
',

    // @codingStandardsIgnoreEnd
  );

  $config = array();

  foreach ($rules as $name => $data) {
    $config[$name] = rules_import($data);
  }

  return $config;
}
