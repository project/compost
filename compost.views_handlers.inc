<?php
/**
 * @file
 * Views handlers.
 */


// @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validclassname_startwithcaptial,style_class_names
class views_handler_filter_dtype extends views_handler_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Data type');
      $this->value_options = _compost_views_dtype_options();
    }
  }
}


// @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validclassname_startwithcaptial,style_class_names
class views_handler_field_variable extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function init(&$view, &$options) {
    parent::init($view, $options);
    $this->additional_fields[] = 'dtype';
    $this->additional_fields[] = 'value';
    $this->additional_fields[] = 'value_i';
    $this->additional_fields[] = 'value_f';
    $this->additional_fields[] = 'value_b';
    $this->additional_fields[] = 'value_t';
  }

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function render($values) {
    $dtype = $this->get_value($values, 'dtype');
    switch ($dtype) {
      case 's':
        $value = $this->get_value($values, "value");
        $value = $this->sanitize_value($value);
        break;

      case 'i':
      case 'f':
        $value = $this->get_value($values, "value_$dtype");
        $value = $this->sanitize_value($value);
        break;

      case 'b':
        $value = $this->get_value($values, "value_b") ?
          t('True') : t('False');
        break;

      case 't':
        $value = format_date($this->get_value($values, "value_t"),
                             'short');
        break;

      case 'v':
        $value = $this->get_value($values, "value");
        $value = $this->sanitize_value($value);
        break;

      default:
        $value = t('Unsuported data type "@dtype"',
                   array('@dtype' => $dtype));
    }

    return $value;
  }
}


// @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validclassname_startwithcaptial,style_class_names
class views_handler_filter_compost_status extends views_handler_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Site status');
      $this->value_options = _compost_views_status_options();
    }
  }
}


// @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validclassname_startwithcaptial,style_class_names
class views_handler_filter_group extends views_handler_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function init(&$view, &$options) {
    parent::init($view, $options);
    $this->value_form_type = 'select';
    $this->value_options = array();
    $query = db_select('compost_group', 'dg')
      ->fields('dg', array('gid', 'name'))
      ->orderBy('name');

    // HACK - limit options based on base table.
    switch ($view->base_table) {
      case 'compost_alert':
      case 'compost_command':
      case 'compost_variable':
        $query->join($view->base_table, 'dbt', 'dbt.gid = dg.gid');
    }

    $res = $query->execute();
    foreach ($res as $row) {
      $this->value_options[$row->gid] = $row->name;
    }
  }
}


// @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validclassname_startwithcaptial,style_class_names
class views_handler_field_link extends views_handler_field_entity {

  // @ignore sniffer_namingconventions_validvariablename_lowercamelname
  public $default_text;

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function option_definition() {
    $options = parent::option_definition();
    $options['text'] = array(
      'default' => '',
      'translatable' => TRUE,
    );

    $options['hide_empty']['default'] = TRUE;

    return $options;
  }


  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function options_form(&$form, &$form_state) {
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
    parent::options_form($form, $form_state);

    $form['alter']['path'] = array('#access' => FALSE);
    $form['alter']['external'] = array('#access' => FALSE);
  }

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function get_url($values) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function render($values) {
    $url = $this->get_url($values);
    if ($url) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = $url['path'];
      $this->options['alter']['query'] = drupal_get_destination() +
        $url['query'];
      $out = (!empty($this->options['text']) ?
              $this->options['text'] : $this->default_text);
    }
    else {
      $out = '';
    }
    return $out;
  }
}


// @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validclassname_startwithcaptial,style_class_names
class views_handler_field_command_link_run extends views_handler_field_link {

  // @ignore sniffer_namingconventions_validvariablename_lowercamelname
  public $allow_run = FALSE;

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function init(&$view, &$options) {
    parent::init($view, $options);
    $this->additional_fields[] = 'nid';
    $this->additional_fields[] = 'ckey';
    $this->default_text = t('run');
    $this->allow_run = user_access('run compost commands');
  }

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function get_url($values) {
    $nid = $this->aliases['nid'];
    $ckey = $this->aliases['ckey'];
    return $this->allow_run ?
      compost_make_command_url($values->$nid, $values->$ckey) : NULL;
  }
}


// @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validclassname_startwithcaptial,style_class_names
class views_handler_field_status_link_override extends views_handler_field_link {

  // @ignore sniffer_namingconventions_validvariablename_lowercamelname
  public $allow_override = FALSE;

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function init(&$view, &$options) {
    parent::init($view, $options);
    $this->additional_fields[] = 'status';
    $this->additional_fields[] = 'overriden';
    $this->default_text = t('override');
    $this->allow_override = user_access('override compost site status');
  }

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function get_url($values) {
    $status = $this->aliases['status'];
    $overriden = $this->aliases['overriden'];
    return ($values->$status != COMPOST_STATUS_OK
            && !$values->$overriden
            && $this->allow_override) ?
      compost_make_override_url($values->nid) : NULL;
  }
}


// @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validclassname_startwithcaptial,style_class_names
class views_handler_field_status_link_reset_override extends views_handler_field_link {

  // @ignore sniffer_namingconventions_validvariablename_lowercamelname
  public $allow_reset = FALSE;

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function init(&$view, &$options) {
    parent::init($view, $options);
    $this->additional_fields[] = 'status';
    $this->additional_fields[] = 'overriden';
    $this->default_text = t('reset override');
    $this->allow_reset = user_access('reset compost site status');
  }

  /**
   * {@inheritdoc}
   */
  // @ignore sniffer_files_linelength_toolong,sniffer_namingconventions_validfunctionname_scopenotlowercamel, sniffer_commenting_functioncomment_missing
  public function get_url($values) {
    $status = $this->aliases['status'];
    $overriden = $this->aliases['overriden'];
    return ($values->$status != COMPOST_STATUS_OK
            && $values->$overriden
            && $this->allow_reset) ?
      compost_make_reset_override_url($values->nid) : NULL;
  }
}
