Introduction
============

Command Post is an remote monitor and control system for Drupal
sites. After you register your sites to a Command Post site, you will
be alerted of exceptional conditions, such as pending database
updates, file system permissions, etc. You can also remotely monitor
several aspects of your Drupal sites, such as installed modules (and
their versions), PHP version, and other interesting PHP
variables. Lastly, you can run remote commands on your Drupal sites,
such as flush caches and run cron.


Main Features
-------------

* Sites are nodes, so you can annotate your sites with additional
  metadata, such as contact information, company department, etc. In
  fact, you can do anything that can be done with nodes.

* Site metadata are Drupal entities. In other words, all sites alerts,
  variables, and commands, are stored as entities, and so can be used
  in views, rules, and in any other way you can use an entity in.

* The module integrates nicely with Views. Since metadata are Drupal
  entities, you can relate your sites with alerts, status, commands,
  and variables. Want to know which site runs and older version of
  PHP? What about a view of all of your sites that are currently in
  maintenance mode? Perhaps you need a view to list all sites where
  cron didn't run in the last couple hours, so you can run cron
  manually (which can be done with a single click on the very same
  view, BTW)?

* Rules integration -- install the Rules module and you will be able
  to some pretty nice things, such as:

  + Send notifications whenever some alert is issued or cleared for a
  site. By using Rules, you can send out e-mails, SMS, IMs, smoke
  signals, or any other notification mechanism that Rules support (or
  that you can write PHP to make it to)

  + Check sites and flag those with exceptional conditions. For
  example, you can raise an alert if a module with a insecure version
  is found on one of your sites. Or you can be mailed whenever someone
  put a site in maintenance mode.

  + The possibilites are endless!

* It is secure. Command Post use strong criptography to securely
  communicate with your sites.

* It's extensible. You can develop your own modules to implement API
  hooks to easily export new alert, commands, and variables.



Requirements
============
Command Post requires the following modules:

* Views (https://www.drupal.org/project/views)

* Entity API (https://www.drupal.org/project/entity)

* Imagecache External (https://www.drupal.org/project/entity)



Recommended Modules
===================
* Rules (https://www.drupal.org/project/rules)

  Though not a hard requirement, the Rules module is highly
  recommended, as many useful things that you might want to do (such
  as sending out notifications) are done using Rules.



Installation
============
See INSTALL.txt.



Troubleshooting
===============
Visit the issue queue at https://www.drupal.org/project/issues/compost
to search for help.
