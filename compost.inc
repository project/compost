<?php
/**
 * @file
 * General functions and constants for the Command Post module.
 */


// These are the status a site can have in the database.
define('COMPOST_STATUS_UNKNOWN', -1);
define('COMPOST_STATUS_OK', 0);
define('COMPOST_STATUS_NOTICE', 10);
define('COMPOST_STATUS_FAIL', 20);


/**
 * Filters undesired markup from text received from agents.
 */
function compost_filter_xss($string) {
  // @todo Use filter_dom_load() to filter out/add base URL from
  //   links, instead of just removing them.
  return filter_xss($string,
                    array(
                      'em', 'strong', 'cite', 'blockquote',
                      'code', 'ul', 'ol', 'li', 'dl', 'dt', 'dd',
                    ));
}


/**
 * Creates a callback URL path.
 */
function _compost_make_url($path) {
  return array(
    'path' => $path,
    'query' => array('token' => drupal_get_token($path)),
  );
}


/**
 * Creates an URL path for running commands.
 */
function compost_make_command_url($nid, $ckey) {
  return _compost_make_url("compost/run/$nid/$ckey");
}


/**
 * Creates an URL path for overriding a site status.
 */
function compost_make_override_url($nid) {
  return _compost_make_url("compost/override/$nid");
}


/**
 * Creates an URL path for reseting a site status.
 */
function compost_make_reset_override_url($nid) {
  return _compost_make_url("compost/reset/$nid");
}


/**
 * Creates an URL path for updating a site.
 */
function compost_make_update_url($nid) {
  return _compost_make_url("compost/update/$nid");
}


/**
 * Runs a command in a site.
 */
function compost_run($node, $cmd) {
  $old_status = compost_find_site_status($node->nid);
  if (!$old_status) {
    watchdog('compost',
             'Tried to run command %command on %site with no status',
             array(
               '%command' => $cmd->ckey,
               '%site' => $node->title,
             ),
             WATCHDOG_CRITICAL);
    drupal_set_message(t('Could not run command on %site because no information for this site is currently available.',
                         array('%site' => $node->title)),
                       'error');
    return;
  }

  $msg = new CompostMessage();

  $msg->addRequest('run', array($cmd->ckey));

  $svc = compost_get_protocol_service();

  try {
    $resp = $svc->send($node, $msg);
  }
  catch (CompostProtocolException $ex) {
    $message = $ex->getMessage();
    compost_delete_alerts($node->nid);
    _compost_add_critical_alert($node,
                             'compost.compost.run_failed',
                             t('Could not run command'),
                             $message);
    compost_check_site($node);
    drupal_set_message(t('Could not run command on %site: %message',
                         array(
                           '%site' => $node->title,
                           '%message' => $message,
                         )),
                       'error');
    return;
  }

  $result = $resp->getResponse('run');

  $cmdname = $result['name'];

  watchdog('compost',
           'Command %command was run on %site',
           array(
             '%command' => $cmdname,
             '%site' => $node->title,
           ),
           WATCHDOG_INFO);

  $messages = $resp->getMessages();
  if ($messages) {
    // theme_compostagent_messages() already handle sanitizing messages
    // returned from agent, so we really do not need to do anything here.
    // @ignore security_3
    drupal_set_message(t('<p>Command %command was run on %site:</p>!messages',
                         array(
                           '%command' => $cmdname,
                           '%site' => $node->title,
                           '!messages' => theme('compostagent_messages',
                                                array('messages' => $messages)),
                         )));
  }
  else {
    drupal_set_message(t('Command %command was run on %site.',
                         array(
                           '%command' => $cmdname,
                           '%site' => $node->title)
                       ));
  }

  module_invoke('rules', 'invoke_event', 'compost_after_command', $node, $cmd);
}


/**
 * Parse a version and returns a version structure.
 *
 * Version structures are arrays with the following elements:
 *  - core - core version
 *  - major - major version
 *  - minor - minor version
 *  - patch - patch level
 *  - tag - version tag
 *  - tagn - version tag increment
 */
function compost_parse_version($version) {

  $res = array(
    'core' => NULL,
    'major' => NULL,
    'minor' => NULL,
    'patch' => NULL,
    'tag' => NULL,
    'tagn' => NULL,
  );

  if (!$version) {
    return $res;
  }

  if (preg_match('/^(\d+)\.x-(\d+)\.(\d+)(?:-(unstable|alpha|beta|rc)(\d+))?$/', $version, $matches)) {
    // Standard x.x-y.y-zzzn version (e.g. 7.x-1.2-beta2).
    $res['core'] = intval($matches[1]);
    $res['major'] = intval($matches[2]);
    $res['minor'] = intval($matches[3]);
    if (count($matches) > 4) {
      $res['tag'] = $matches[4];
      $res['tagn'] = intval($matches[5]);
    }
    else {
      $res['tag'] = $res['tagn'] = NULL;
    }
  }
  elseif (preg_match('/^(\d+)\.x-(\d+)\.(\d+|x)(\+\d+)?-dev$/', $version, $matches)) {
    // x.x-y.y+zz-dev versions.
    $res['core'] = intval($matches[1]);
    $res['major'] = intval($matches[2]);
    if ($matches[3] != 'x') {
      $res['minor'] = intval($matches[3]);
    }
    $res['tag'] = 'dev';
    if (count($matches) > 4) {
      $res['tagn'] = intval($matches[4]);
    }
  }
  elseif (preg_match('/^(\d+)\.(\d+)(?:-(unstable|alpha|beta|rc)(\d+))?$/', $version, $matches)) {
    // Core x.x(-tagn) version.
    $res['core'] = intval($matches[1]);
    $res['major'] = intval($matches[1]);
    $res['minor'] = intval($matches[2]);
    if (count($matches) > 3) {
      $res['tag'] = $matches[3];
      $res['tagn'] = intval($matches[4]);
    }
  }
  elseif (preg_match('/^(\d+)\.(\d+)(\+\d+)?-dev$/', $version, $matches)) {
    // Core x.x+nnn-dev version.
    $res['core'] = intval($matches[1]);
    $res['major'] = intval($matches[1]);
    $res['minor'] = intval($matches[2]);
    $res['tag'] = 'dev';
    if (count($matches) > 3) {
      $res['tagn'] = intval($matches[3]);
    }
  }
  elseif (preg_match('/^(\d+)\.(\d+)\.(\d+)$/', $version, $matches)) {
    // x.x.x versions.
    $res['major'] = intval($matches[1]);
    $res['minor'] = intval($matches[2]);
    $res['patch'] = intval($matches[3]);
  }
  else {
    throw new InvalidArgumentException("Invalid version '$version'");
  }

  return $res;
}


/**
 * Create a compost_variable entity.
 */
function compost_create_variable($value) {
  $var = entity_create('compost_variable', array('dtype' => NULL));

  if (is_array($value)) {
    if (!isset($value['type'])) {
      throw new InvalidArgumentException("Unsupported array type");
    }
    $var->dtype = $value['type'];
    $value = $value['value'];
  }
  elseif (is_int($value)) {
    $var->dtype = 'i';
  }
  elseif (is_float($value)) {
    $var->dtype = 'f';
  }
  elseif (is_string($value)) {
    $var->dtype = 's';
  }
  elseif (is_bool($value)) {
    $var->dtype = 'b';
  }
  else {
    throw new InvalidArgumentException('Unsupported "' . gettype($value)
                                       . '" variable value');
  }

  switch ($var->dtype) {
    case 'i':
      $var->value_i = $value;
      break;

    case 'f':
      $var->value_f = $value;
      break;

    case 's':
      // Nothing to do.
      break;

    case 'b':
      $var->value_b = $value;
      break;

    case 't':
    case 'timestamp':
      $var->dtype = 't';
      $var->value_t = $value;
      break;

    case 'v':
    case 'version':
      $version = compost_parse_version($value);

      $var->dtype = 'v';
      $var->value_v_core = $version['core'];
      $var->value_v_major = $version['major'];
      $var->value_v_minor = $version['minor'];
      $var->value_v_patch = $version['patch'];
      $var->value_v_tag = $version['tag'];
      $var->value_v_tagn = $version['tagn'];
      break;

    default:
      throw new InvalidArgumentException("Unsupported data type '$var->dtype'");
  }

  $var->value = $value;

  return $var;
}


/**
 * Checks if an alert should be ignored.
 *
 * Sets the alert "ignored" flag if the alert name is in the lists of
 * alerts that should be ignored.
 */
function _compost_set_ignored($alert) {
  static $ignore_list = NULL;

  if ($ignore_list === NULL) {
    $ignore_list = array();
    foreach (preg_split('[\r\n]', variable_get('compost_ignored_alerts', '')) as $line) {
      $line = trim($line);
      if ($line) {
        $ignore_list[] = $line;
      }
    }
  }

  $alert->ignored = 0;
  foreach ($ignore_list as $i) {
    if (fnmatch($i, $alert->akey, FNM_NOESCAPE | FNM_CASEFOLD)) {
      $alert->ignored = 1;
      break;
    }
  }
}


/**
 * Save a site node alerts.
 */
function _compost_update_alerts($node, $alerts) {
  if (!$alerts) {
    return;
  }

  foreach ($alerts as $module => $group) {
    foreach ($group as $prefix => $info) {
      $gid = compost_get_group($info['group']);
      foreach ($info['alerts'] as $key => $alinfo) {
        $al = entity_create('compost_alert', array());
        $al->nid = $node->nid;
        $al->akey = "$module.$prefix.$key";
        $al->name = $alinfo['name'];
        $al->changed = REQUEST_TIME;
        if (isset($alinfo['description'])) {
          $al->description = compost_filter_xss($alinfo['description']);
        }
        $al->status = _compost_get_agent_status($alinfo['status']);
        $al->gid = $gid;

        _compost_set_ignored($al);

        $al->save();
      }
    }
  }
}


/**
 * Save a site node commands.
 */
function _compost_update_commands($node, $commands) {
  if (!$commands) {
    return;
  }

  foreach ($commands as $module => $group) {
    foreach ($group as $prefix => $info) {
      $gid = compost_get_group($info['group']);
      foreach ($info['commands'] as $key => $command) {
        $cmd = entity_create('compost_command', array());

        $cmd->nid = $node->nid;
        $cmd->ckey = "$module.$prefix.$key";
        $cmd->name = $command['name'];
        if (isset($command['description'])) {
          $cmd->description = compost_filter_xss($command['description']);
        }
        $cmd->gid = $gid;
        $cmd->save();
      }
    }
  }
}


/**
 * Save a site node variables.
 */
function _compost_update_variables($node, $variables) {
  if (!$variables) {
    return;
  }

  foreach ($variables as $module => $group) {
    foreach ($group as $prefix => $info) {
      $gid = compost_get_group($info['group']);
      foreach ($info['variables'] as $key => $varinfo) {
        try {
          $var = compost_create_variable($varinfo['value']);
        }
        catch (Exception $ex) {
          watchdog('compost',
                   'Variable error from %site: %error',
                   array(
                     '%site' => $node->title,
                     '%error' => $ex->getMessage(),
                   ),
                   WATCHDOG_ERROR);
          continue;
        }

        $var->nid = $node->nid;
        $var->vkey = "$module.$prefix.$key";
        $var->name = $varinfo['name'];
        $var->changed = REQUEST_TIME;
        $var->gid = $gid;
        $var->save();
      }
    }
  }
}


/**
 * Adds a critical alert.
 *
 * This is used internally to create alerts generated internally by
 * the Command Post site.
 */
function _compost_add_critical_alert($node, $akey, $name, $description) {
  $alert = entity_create('compost_alert', array());
  $alert->nid = $node->nid;
  $alert->akey = $akey;
  $alert->name = $name;
  $alert->description = $description;
  $alert->status = COMPOST_STATUS_FAIL;
  $alert->changed = REQUEST_TIME;
  $alert->ignored = FALSE;
  $alert->gid = compost_get_group(t('Command Post'));
  // NB: Do not call _compost_set_ignored() here -- critical alerts
  // should never be ignored.
  $alert->save();
}


/**
 * Deletes a site's alerts, commands, and variables.
 */
function compost_flush_site($node) {
  compost_delete_alerts($node->nid);
  compost_delete_commands($node->nid);
  compost_delete_variables($node->nid);
}


/**
 * Returns the cipher factory to be used for creating cipher objects.
 */
function compost_get_cipher_factory() {
  if (function_exists('mcrypt_decrypt')) {
    $cipher_factory = new CompostMCryptCipherFactory();
  }
  elseif (function_exists('openssl_encrypt')) {
    $cipher_factory = new CompostOpenSSLCipherFactory();
  }
  else {
    throw new CompostProtocolException(t('Neither mcrypt nor openssl PHP extensions are installed. Cannot encrypt/decrypt!'));
  }
  return $cipher_factory;
}


/**
 * Get the (singleton) Command Post protocol service.
 */
function compost_get_protocol_service() {
  static $svc;

  if (!$svc) {
    $svc = new CompostProtocolService(variable_get('compost_key', ''),
                                      compost_get_cipher_factory());
  }

  return $svc;
}


/**
 * Update a node.
 */
function _compost_update_node($node) {
  compost_flush_site($node);

  // Update commands, alerts, and variables.
  $msg = new CompostMessage();
  $msg->addRequest('commands');
  $msg->addRequest('alerts');
  $msg->addRequest('variables');

  $svc = compost_get_protocol_service();

  try {
    $resp = $svc->send($node, $msg);
  }
  catch (CompostProtocolException $ex) {
    $message = $ex->getMessage();
    watchdog('compost',
             'Protocol error updating %site: %message',
             array(
               '%site' => $node->title,
               '%message' => $message,
             ),
             WATCHDOG_ERROR,
             l(t('view'), 'node/' . $node->nid));

    _compost_add_critical_alert($node,
                             'compost.compost.update_fail',
                             t('Failed to update site'),
                             $message);
    compost_check_site($node);
    return FALSE;
  }

  _compost_update_alerts($node, $resp->getResponse('alerts'));
  _compost_update_commands($node, $resp->getResponse('commands'));
  _compost_update_variables($node, $resp->getResponse('variables'));

  module_invoke('rules', 'invoke_event', 'compost_after_update', $node);

  compost_check_site($node);

  return TRUE;
}


/**
 * Checks all alerts for a site and update its status.
 */
function compost_check_site($node) {
  $old_status = compost_find_site_status($node->nid);
  $is_new = !(bool) $old_status;
  if (!$old_status) {
    $old_status = entity_create('compost_site_status',
                                array(
                                  'nid' => $node->nid,
                                  'status' => COMPOST_STATUS_UNKNOWN,
                                  'last_message' => '',
                                ));
  }
  $new_status = entity_create('compost_site_status',
                              array(
                                'nid' => $node->nid,
                                'status' => COMPOST_STATUS_OK,
                                'changed' => REQUEST_TIME,
                                'updated' => REQUEST_TIME,
                                'overriden' => 0,
                                'last_message' => '',
                              ));

  // Check alerts.
  $alerts = compost_find_alerts($node->nid);

  foreach ($alerts as $alert) {
    if (!$alert->ignored && $alert->status > $new_status->status) {
      $new_status->status = $alert->status;
      $new_status->last_message = $alert->name;

      // Check if we already have a failure. There's no
      // no point in continuing if we have one.
      if ($new_status->status == COMPOST_STATUS_FAIL) {
        break;
      }
    }
  }

  // Now save new status, if changed.
  if (!$new_status->statusEquals($old_status)) {
    watchdog('compost',
             '%site: @old_status @cur_message → @new_status @new_message',
             array(
               '%site' => $node->title,
               '@old_status' => compost_status_name($old_status->status),
               '@cur_message' => $old_status->last_message,
               '@new_status' => compost_status_name($new_status->status),
               '@new_message' => $new_status->last_message,
             ),
             WATCHDOG_NOTICE);

    $new_status->is_new = $is_new;
    $new_status->save();

    module_invoke('rules', 'invoke_event', 'compost_site_status_change',
                  $node, $new_status, $old_status);
  }
  else {
    compost_touch($node);
  }

  return $new_status;
}


/**
 * Updates the "updated" field in the site status record.
 */
function compost_touch($node) {
  db_update('compost_site_status')
    ->fields(array('updated' => REQUEST_TIME))
    ->condition('nid', $node->nid)
    ->execute();
  entity_get_controller('compost_site_status')->resetCache(array($node->nid));
}


/**
 * Returns the numeric status corresponding to statuses returned by agents.
 */
function _compost_get_agent_status($astatus) {
  switch ($astatus) {
    case 'ok':
      $status = COMPOST_STATUS_OK;
      break;

    case 'notice':
      $status = COMPOST_STATUS_NOTICE;
      break;

    case 'fail':
      $status = COMPOST_STATUS_FAIL;
      break;

    default:
      watchdog('compost',
               'Unknow status %status returned from agent',
               array('%status' => $astatus),
               WATCHDOG_ERROR);
      $status = COMPOST_STATUS_FAIL;
  }
  return $status;
}


/**
 * Update a node.
 */
function compost_update_node($node) {
  if (_compost_update_node($node)) {
    $stats = compost_get_site_stats($node);
    drupal_set_message(t('Updated %site &mdash; %commands, %alerts, %variables.',
                         array(
                           '%site' => $node->title,
                           '%commands' => format_plural($stats['commands'], '1 command', '@count commands'),
                           '%alerts' => format_plural($stats['alerts'], '1 alert', '@count alerts'),
                           '%variables' => format_plural($stats['variables'], '1 variable', '@count variables'),
                         )));
  }
  else {
    drupal_set_message(t('<p>Failed to update %site.',
                         array('%site' => $node->title)),
                       'error');
  }
}


/**
 * Implements hook_query_TAG_alter().
 *
 * Join nodes with theirs corresponding site status, so that we can
 * order the query by update time.
 */
function compost_query_compost_order_by_updated_alter(QueryAlterableInterface $query) {
  $query->leftJoin('compost_site_status', 'css', 'node.nid = css.nid');
  $query->orderBy('css.updated');
}


/**
 * Update all nodes.
 */
function compost_update_all_nodes($limit = 0) {

  $query  = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->fieldCondition('compost_url', 'url', 'NULL', '!=')
    ->addMetaData('account', user_load(1));

  if ($limit) {
    $query->range(0, $limit);
    $query->addTag('compost_order_by_updated');
  }

  $result = $query->execute();
  if (isset($result['node'])) {
    foreach (array_keys($result['node']) as $nid) {
      _compost_update_node(node_load($nid));
    }
  }
}


/**
 * Maps a numeric status to the corresponding status name.
 */
function compost_status_map() {
  return array(
    COMPOST_STATUS_UNKNOWN => compost_status_name(COMPOST_STATUS_UNKNOWN),
    COMPOST_STATUS_OK => compost_status_name(COMPOST_STATUS_OK),
    COMPOST_STATUS_NOTICE => compost_status_name(COMPOST_STATUS_NOTICE),
    COMPOST_STATUS_FAIL => compost_status_name(COMPOST_STATUS_FAIL),
  );
}


/**
 * Maps the internal data type specifier to human-readable type names.
 */
function compost_dtype_map() {
  return array(
    'i' => t('Integer'),
    'f' => t('Float'),
    's' => t('String'),
    'b' => t('Boolean'),
    't' => t('Unix timestamp'),
    'v' => t('Version'),
  );
}


/**
 * Maps version suffixes to more human-friendly names.
 */
function compost_version_tag_map() {
  return array(
    'dev' => t('Development'),
    'unstable' => t('Unstable'),
    'alpha' => t('Alpha'),
    'beta' => t('Beta'),
    'rc' => t('Release candidade'),
  );
}


/**
 * Returns the group entity corresponding to a group name.
 *
 * The entity will be created if no such group exists, so it is
 * guaranteed that this funcition will always return a group.
 */
function compost_get_group($name) {
  static $cache = array();

  if (!isset($cache[$name])) {
    $row = db_select('compost_group', 'dg')
      ->fields('dg', array('gid'))
      ->condition('name', $name)
      ->execute()
      ->fetchAssoc();
    if ($row) {
      $cache[$name] = $row['gid'];
    }
    else {
      $cache[$name] = db_insert('compost_group')
        ->fields(array('name' => $name))
        ->execute();
    }
  }

  return $cache[$name];
}


/**
 * Returns all alerts for a node.
 *
 * @param int $nid
 *   The node ID.
 */
function compost_find_alerts($nid) {
  $alerts = array();

  $query = new EntityFieldQuery();
  $res = $query->entityCondition('entity_type', 'compost_alert')
    ->propertyCondition('nid', $nid)
    ->propertyOrderBy('status', 'DESC')
    ->execute();

  if (isset($res['compost_alert'])) {
    $alerts = array_values(entity_load('compost_alert',
                                       array_keys($res['compost_alert'])));
  }

  return $alerts;
}


/**
 * Deletes all site data of a specific type.
 */
function _compost_delete_site_data($nid, $type) {
  $query = new EntityFieldQuery();

  $res = $query->entityCondition('entity_type', $type)
    ->propertyCondition('nid', $nid)
    ->execute();

  if (!empty($res[$type])) {
    entity_delete_multiple($type, array_keys($res[$type]));
  }
}


/**
 * Deletes all alerts of a node.
 */
function compost_delete_alerts($nid) {
  _compost_delete_site_data($nid, 'compost_alert');
}


/**
 * Deletes all commands of a node.
 */
function compost_delete_commands($nid) {
  _compost_delete_site_data($nid, 'compost_command');
}


/**
 * Deletes all variables of a node.
 */
function compost_delete_variables($nid) {
  _compost_delete_site_data($nid, 'compost_variable');
}


/**
 * Deletes a node's status.
 */
function compost_delete_site_status($nid) {
  _compost_delete_site_data($nid, 'compost_site_status');
}


/**
 * Converts a numeric site status to a human-friendly name.
 */
function compost_status_name($status) {
  switch ($status) {
    case COMPOST_STATUS_UNKNOWN:
      $name = t('Unknown');
      break;

    case COMPOST_STATUS_OK:
      $name = t('Ok');
      break;

    case COMPOST_STATUS_NOTICE:
      $name = t('Notice');
      break;

    case COMPOST_STATUS_FAIL:
      $name = t('Fail');
      break;

    default:
      $name = t('Unknown status %status', array('%status' => $status));
  }
  return $name;
}


/**
 * Sets or resets the overriden status of a node.
 *
 * @param node $node
 *   The node to be acted upon.
 *
 * @param bool $overriden
 *   The new overriden status.
 */
function compost_set_overriden($node, $overriden) {
  db_update('compost_site_status')
    ->fields(array('overriden' => (int) (bool) $overriden))
    ->condition('nid', $node->nid)
    ->execute();
  entity_get_controller('compost_site_status')->resetCache(array($node->nid));
  if ($overriden) {
    watchdog('compost',
             '%site overriden status set',
             array('%site' => $node->title));
    module_invoke('rules', 'invoke_event', 'compost_overriden', $node);
  }
  else {
    watchdog('compost',
             '%site overriden status reset',
             array('%site' => $node->title));
    module_invoke('rules', 'invoke_event', 'compost_overriden_reset', $node);
  }
}


/**
 * Finds a site status.
 */
function compost_find_site_status($nid, $reset = FALSE) {
  $query = new EntityFieldQuery();
  $res = $query->entityCondition('entity_type', 'compost_site_status')
    ->propertyCondition('nid', $nid)
    ->addMetaData('account', user_load(1))
    ->execute();
  if (isset($res['compost_site_status'])) {
    $status = entity_load('compost_site_status',
                          array_keys($res['compost_site_status']),
                          array(), $reset);
    $status = reset($status);
  }
  else {
    $status = NULL;
  }
  return $status;
}


/**
 * Returns statistics about a site node.
 */
function compost_get_site_stats($node) {
  $stats = array();

  $q = db_select('compost_alert', 'da')
    ->condition('nid', $node->nid);
  $q->addExpression('COUNT(*)', 'count');
  $stats['alerts'] = $q->execute()->fetchField();

  $q = db_select('compost_command', 'dc')
    ->condition('nid', $node->nid);
  $q->addExpression('COUNT(*)', 'count');
  $stats['commands'] = $q->execute()->fetchField();

  $q = db_select('compost_variable', 'dv')
    ->condition('nid', $node->nid);
  $q->addExpression('COUNT(*)', 'count');
  $stats['variables'] = $q->execute()->fetchField();

  return $stats;
}


/**
 * Compare hashes in fixed-time.
 *
 * Will call hash_equals() if supported by PHP.
 */
function compost_hash_equals($known_string, $user_string) {
  if (function_exists('hash_equals')) {
    return hash_equals($known_string, $user_string);
  }

  $len_known = strlen($known_string);
  $len_user = strlen($user_string);

  if ($len_known != $len_user) {
    return FALSE;
  }

  $nulls = $known_string ^ $user_string;
  $result = 0;
  for ($i = 0; $i < $len_known; $i++) {
    $result |= ord($nulls[$i]);
  }

  return !$result;
}


/**
 * Derive a key from a password using PBKDF2.
 *
 * Based on
 * https://github.com/defuse/password-hashing/blob/master/PasswordHash.php
 * PBKDF2 implementation.
 *
 * @see http://php.net/manual/en/function.hash-pbkdf2.php
 */
function compost_pbkdf2($algorithm, $password, $salt, $count,
                        $key_length = 0, $raw_output = FALSE) {
  $algorithm = strtolower($algorithm);
  if (!in_array($algorithm, hash_algos(), TRUE)) {
    throw new RuntimeException('PBKDF2 ERROR: Invalid hash algorithm:' . $algorithm);
  }
  if ($count <= 0 || $key_length <= 0) {
    throw new RuntimeException('PBKDF2 ERROR: Invalid parameters.');
  }
  if (function_exists("hash_pbkdf2")) {
    // The output length is in NIBBLES (4-bits) if $raw_output is false!
    if (!$raw_output) {
      $key_length = $key_length * 2;
    }
    return hash_pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output);
  }
  $hash_length = strlen(hash($algorithm, "", TRUE));
  $block_count = ceil($key_length / $hash_length);
  $output = "";
  for ($i = 1; $i <= $block_count; $i++) {
    // $i encoded as 4 bytes, big endian.
    $last = $salt . pack("N", $i);
    // First iteration.
    $last = $xorsum = hash_hmac($algorithm, $last, $password, TRUE);
    // Perform the other $count - 1 iterations.
    for ($j = 1; $j < $count; $j++) {
      $xorsum ^= ($last = hash_hmac($algorithm, $last, $password, TRUE));
    }
    $output .= $xorsum;
  }
  if ($raw_output) {
    return substr($output, 0, $key_length);
  }
  else {
    return bin2hex(substr($output, 0, $key_length));
  }
}
