<?php
/**
 * @file
 * Rules integration functions.
 */


/**
 * Return the override status of a status entity.
 */
function compost_cond_status_overriden($status) {
  return $status->overriden;
}


/**
 * Implements hook_rules_condition_info().
 */
function compost_rules_condition_info() {
  return array(
    'compost_cond_status_overriden' => array(
      'label' => t('Site status is overriden'),
      'group' => t('Command Post'),
      'parameter' => array(
        'status' => array(
          'label' => t('Site status'),
          'type' => 'compost_site_status',
        ),
      ),
    ),
    'compost_node_is_site' => array(
      'label' => t('Node contains site data'),
      'group' => t('Command Post'),
      'parameter' => array(
        'node' => array(
          'label' => t('Node'),
          'type' => 'node',
        ),
      ),
    ),
  );
}


/**
 * Implements hook_rules_event_info().
 */
function compost_rules_event_info() {

  $defaults = array(
    'group' => t('Command Post'),
  );

  $events = array(
    'compost_site_status_change' => $defaults + array(
      'label' => t('After a site status has changed'),
      'variables' => rules_events_node_variables(t('node')) +
      array(
        'new_status' => array(
          'type' => 'compost_site_status',
          'label' => t('new site status'),
        ),
        'old_status' => array(
          'type' => 'compost_site_status',
          'label' => t('old site status'),
        ),
      ),
    ),
    'compost_after_command' => $defaults + array(
      'label' => t('After a remote command has been run'),
      'variables' => rules_events_node_variables(t('node')) +
      array(
        'command' => array(
          'type' => 'compost_command',
          'label' => t('remote command'),
        ),
      ),
    ),
    'compost_after_update' => $defaults + array(
      'label' => t('After updating a site'),
      'variables' => rules_events_node_variables(t('node')),
    ),
    'compost_overriden' => $defaults + array(
      'label' => t('After a site status has been overriden'),
      'variables' => rules_events_node_variables(t('node')),
    ),
    'compost_overriden_reset' => $defaults + array(
      'label' => t('After an overriden site status has been reset'),
      'variables' => rules_events_node_variables(t('node')),
    ),
  );

  return $events;
}


/**
 * Implements hook_rules_action_info().
 */
function compost_rules_action_info() {

  $defaults = array(
    'group' => t('Command Post'),
  );

  $actions = array(
    'compost_run_command' => $defaults + array(
      'label' => t('Run a remote command'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Site node'),
        ),
        'command' => array(
          'type' => 'compost_command',
          'label' => t('Command'),
        ),
      ),
      'base' => 'compost_run',
    ),
    'compost_update_node' => $defaults + array(
      'label' => t('Update a site'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Site node'),
        ),
      ),
    ),
    'compost_fetch_alert' => $defaults + array(
      'label' => t('Fetch a site alert by key'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Site node'),
        ),
        'akey' => array(
          'type' => 'text',
          'label' => t('Alert key'),
        ),
      ),
      'provides' => array(
        'alert_fetched' => array(
          'type' => 'compost_alert',
          'label' => t('Fetched alert'),
        ),
      ),
      'base' => '_compost_rules_fetch_alert',
    ),
    'compost_fetch_command' => $defaults + array(
      'label' => t('Fetch a site command by key'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Site node'),
        ),
        'ckey' => array(
          'type' => 'text',
          'label' => t('Command key'),
        ),
      ),
      'provides' => array(
        'command_fetched' => array(
          'type' => 'compost_command',
          'label' => t('Fetched command'),
        ),
      ),
      'base' => '_compost_rules_fetch_command',
    ),
    'compost_fetch_variable' => $defaults + array(
      'label' => t('Fetch a site variable by key'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Site node'),
        ),
        'vkey' => array(
          'type' => 'text',
          'label' => t('Variable key'),
        ),
      ),
      'provides' => array(
        'variable_fetched' => array(
          'type' => 'compost_variable',
          'label' => t('Fetched variable'),
        ),
      ),
      'base' => '_compost_rules_fetch_variable',
    ),
    'compost_create_alert' => $defaults + array(
      'label' => t('Create a local alert'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Site node'),
        ),
        'key' => array(
          'type' => 'token',
          'label' => t('Key suffix'),
          'description' => t('Rules will always prepend <span class="compost-key">local.rules.</span> to this key to form the final alert key. For example, if you input <em>foobar</em> here, the generated alert will get the key <span class="compost-key">local.rules.foobar</span>.'),
        ),
        'name' => array(
          'type' => 'text',
          'label' => t('Name'),
        ),
        'description' => array(
          'type' => 'text',
          'label' => t('Description'),
          'optional' => TRUE,
          'allow null' => TRUE,
        ),
        'status' => array(
          'type' => 'integer',
          'label' => t('Status'),
          'options list' => 'compost_status_map',
        ),
      ),
      'provides' => array(
        'alert_created' => array(
          'type' => 'compost_alert',
          'label' => t('Created alert'),
          'save' => TRUE,
        ),
      ),
      'base' => '_compost_rules_create_alert',
    ),
  );

  return $actions;
}


/**
 * Trigger function for the "compost_create_alert" rule.
 */
function _compost_rules_create_alert($node, $key, $name, $description, $status) {

  return array(
    'alert_created' => entity_create('compost_alert',
                                     array(
                                       'nid' => $node->nid,
                                       'akey' => "local.rules.$key",
                                       'name' => $name,
                                       'description' => $description,
                                       'status' => $status,
                                       'changed' => REQUEST_TIME,
                                       'ignored' => FALSE,
                                       'gid' => compost_get_group(t('Local')),
                                     )),
  );
}


/**
 * Generic function for fetching site data.
 */
function _compost_rules_fetch_data($node, $key, $key_property, $type) {

  $query = new EntityFieldQuery();
  $res = $query->entityCondition('entity_type', $type)
    ->propertyCondition('nid', $node->nid)
    ->propertyCondition($key_property, $key)
    ->execute();

  if (!isset($res[$type])) {
    throw new RulesEvaluationException('Unable to load @entity with key "@key" for %site',
                                       array(
                                         '@entity' => $type,
                                         '@key' => $key,
                                         '%site' => $node->title,
                                       ));
  }

  $data = entity_load($type, array_keys($res[$type]));
  return reset($data);
}


/**
 * Trigger function for the "compost_fetch_alert" rule.
 */
function _compost_rules_fetch_alert($node, $akey) {
  return array(
    'alert_fetched' => _compost_rules_fetch_data($node,
                                              $akey,
                                              'akey',
                                              'compost_alert'),
  );
}


/**
 * Trigger function for the "compost_fetch_command" rule.
 */
function _compost_rules_fetch_command($node, $ckey) {
  return array(
    'command_fetched' => _compost_rules_fetch_data($node,
                                                $ckey,
                                                'ckey',
                                                'compost_command'),
  );
}


/**
 * Trigger function for the "compost_fetch_variable" rule.
 */
function _compost_rules_fetch_variable($node, $vkey) {
  return array(
    'variable_fetched' => _compost_rules_fetch_data($node,
                                                 $vkey,
                                                 'vkey',
                                                 'compost_variable'),
  );
}
