<?php
/**
 * @file
 * Cryptographic classes using Mcrypt.
 */


/**
 * The Mcrypt-based AES cipher.
 */
class CompostMCryptAESCipher extends CompostCipher {
  protected $bits;
  protected $mode;

  /**
   * Constructs the object.
   */
  public function __construct($bits, $mode) {
    assert('$bits == 128');
    assert('$mode == MCRYPT_MODE_CBC');
    $this->bits = $bits;
    $this->mode = $mode;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'aes-' . $this->bits . '-cbc';
  }

  /**
   * Derive a key from a password.
   */
  protected function deriveKey($password) {
    return substr(hash('sha1', $password, TRUE), 0, 16);
  }

  /**
   * {@inheritdoc}
   */
  public function encrypt($data, $password) {
    $key = $this->deriveKey($password);

    // Mcrypt pads with NUL. Let's do our own paddding instead, using PKCS#7.
    $npadding = 16 - (strlen($data) % 16);
    $data .= str_repeat(chr($npadding), $npadding);

    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

    return $iv . mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key,
                                $data, MCRYPT_MODE_CBC, $iv);
  }

  /**
   * {@inheritdoc}
   */
  public function decrypt($data, $password) {
    $key = $this->deriveKey($password);

    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

    $iv = substr($data, 0, $iv_size);
    $data = substr($data, $iv_size);

    $dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key,
                          $data, MCRYPT_MODE_CBC, $iv);

    $dec_len = strlen($dec);
    $padding = ord($dec[$dec_len - 1]);

    return substr($dec, 0, -$padding);
  }
}


/**
 * The Mcrypt-based cipher factory.
 */
class CompostMCryptCipherFactory extends CompostCipherFactory {

  /**
   * {@inheritdoc}
   */
  public function newCipher($algo) {
    if ($algo == 'aes-128-cbc') {
      return new CompostMCryptAESCipher(128, MCRYPT_MODE_CBC);
    }

    throw new CompostProtocolException("Unsupported mcrypt cipher algorithm: $algo");
  }
}
