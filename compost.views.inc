<?php
/**
 * @file
 * Views integration functions.
 */


/**
 * Implements hook_views_data_alter().
 */
function compost_views_data_alter(&$data) {

  $data['compost_site_status']['link_override'] = array(
    'title' => t('Override link'),
    'help' => t('Provide a simple link to override a site status.'),
    'field' => array(
      'handler' => 'views_handler_field_status_link_override',
    ),
  );

  $data['compost_site_status']['link_reset_override'] = array(
    'title' => t('Reset override link'),
    'help' => t('Provide a simple link to reset an overriden site status.'),
    'field' => array(
      'handler' => 'views_handler_field_status_link_reset_override',
    ),
  );

  $data['compost_command']['link_run'] = array(
    'title' => t('Run link'),
    'help' => t('Provide a simple link to run the command on the site.'),
    'field' => array(
      'handler' => 'views_handler_field_command_link_run',
    ),
  );
}
